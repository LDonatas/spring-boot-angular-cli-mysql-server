package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@EnableAutoConfiguration
@ComponentScan(basePackages={"com.example.demo"})
@EnableJpaRepositories(basePackages="com.example.demo.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="com.example.demo.entities")
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

   /* @Bean
    CommandLineRunner init(ProductRepository productRepository) {
        return args -> {
            Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(title -> {
                Product product = new Product(title, title.length(), title.length() -1, title.length() + 2 );
                productRepository.save(product);
            });
            productRepository.findAll().forEach(System.out::println);
        };
    }*/

}
